package com.jacoco.test;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class TestApplicationTests {

    @Test
    void contextLoads() {
    }

    @Test
    public void whenEmptyString_thenAccept() {
        Palindrome palindromeTester = new Palindrome();
        Assert.assertTrue(palindromeTester.isPalindrome(""));
    }

    @Test
    public void sumPositiveTest() {
        Palindrome palindromeTester = new Palindrome();
        Assert.assertEquals(palindromeTester.sumNumber(10,1),20,0.5);
    }

    @Test
    public void sumNegativeTest() {
        Palindrome palindromeTester = new Palindrome();
        Assert.assertEquals(palindromeTester.sumNumber(10,-11),-100,0.5);
    }

}
