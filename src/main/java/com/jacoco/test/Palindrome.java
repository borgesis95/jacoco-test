    package com.jacoco.test;

    public class Palindrome {

        public boolean isPalindrome(String inputString) {
            if (inputString.length() == 0) {
                return true;
            } else {
                char firstChar = inputString.charAt(0);
                char lastChar = inputString.charAt(inputString.length() - 1);
                String mid = inputString.substring(1, inputString.length() - 1);
                return (firstChar == lastChar) && isPalindrome(mid);
            }
        }


        public float sumNumber(float a, float b) {
            float result = a + b;

            if(result >0) {
                result = result;
            } else {
                result =result*100;
            }

            return result;
        }

    }
